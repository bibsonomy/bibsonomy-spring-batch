Batch application and adminpanel for managing, monitoring, scheduling and processing batch jobs. In future the application should replace the old batch-scripts which are implemented in perl and combine them in one java application. This project is in development!

Used frameworks:

* Spring Batch / Spring Batch Admin
* Quartz
* MyBatis 

### Prepare ###

* At first download the maven project.
* You need a setuped bibsonomy database. You can find the schema under *bibsonomy/bibsonomy-database* or you can use your own database.
* You need another database for the application.
* After you setuped the databases you have to edit the configuration files under *src/main/resources*. You have to add in the *mybatis-master-config.xml* and *mybatis-slave-config.xml* the connection to the database.
* In *batch-mysql.properties* you add the other database configuration. It is important that you set *batch.data.source.init* on true for the first run or you will get an error. This setups the tables in the database. After the first run you can set *batch.data.source.init* on false or it will always clear the database. This flag is important to save the data of the application permanently.

### How to run ###

* Run *mvn clear* and *mvn install* to compile the project. This will generate *SpringBatchAdmin.war*. With a Tomcat Server (or what you want) you can deploy this artifact.
* Open the website. For example: *http://localhost:8080/jobs*
* In the jobs list or in the quartz list you can run the jobs.

### Add new batch jobs ###

Currently are only two jobs (*batch_popular_masterslave.pl* and *batch_tagtag_masterslave.pl*) implemented from: *https://bitbucket.org/bibsonomy/bibsonomy-batch/src/7b544a343399/src/build/batch_scripts/?at=default* 

If you want to add new jobs in java you have to add it first in the class *de.unihannover.weblab.job.JobConfiguration*. You have to implement a new bean with the new job name and the associated method which return the job. This can be like:


```
#!java
    @Bean (name = "tagTagMasterslaveJob")
    public Job tagTagMasterslaveJob() {

        TagTagMasterslaveTasklet task = new TagTagMasterslaveTasklet();
        Step popularMasterslaveJobStep = steps.get("tagTagMasterslaveJobStep")
                .tasklet(task)
                .build();

        return this.jobs.get("tagTagMasterslaveJob").incrementer(task).
                flow(popularMasterslaveJobStep).
                end().build();
    }

```
You have to implement a tasklet and a incrementer for the job. You can do it in the package *de.unihannover.weblab.tasks*.
The tasklet processes the statements to database and the incrementer processes the job parameter.