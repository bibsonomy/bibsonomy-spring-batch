package de.unihannover.weblab.service;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;

public class MyBatisUtil {
    private static SqlSessionFactory slave;
    private static SqlSessionFactory master;

    private MyBatisUtil() {
    }

    /**
     * Initialize the master and slave connection
     */
    static {
        Reader reader = null;
        try {
            reader = Resources.getResourceAsReader("mybatis-slave-config.xml");
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        slave = new SqlSessionFactoryBuilder().build(reader);


        reader = null;
        try {
            reader = Resources.getResourceAsReader("mybatis-master-config.xml");
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        master = new SqlSessionFactoryBuilder().build(reader);
    }

    /**
     * Return the slave connection
     * @return
     */
    public static SqlSessionFactory getSlaveSqlSessionFactory() {
        return slave;
    }

    /**
     * Return the master connection
     * @return
     */
    public static SqlSessionFactory getMasterSqlSessionFactory() {
        return master;
    }
}