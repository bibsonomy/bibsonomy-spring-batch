package de.unihannover.weblab.tasks.popularmasterslave.mappers;

import de.unihannover.weblab.tasks.popularmasterslave.domain.Pair;

import java.util.List;
import java.util.Map;

/**
 * Mapper for the PopularMasterslaveJob.
 * It is important that this class is included in the mybatis config
 * and in the mapper xml file.
 *
 *
 * Created by sinan on 16/11/15.
 */
public interface PopularMasterSlaveMapper {

    public List<Pair> select_bookmark(Map map);

    public Map get_first_bookmark(Map map);

    public void insert_bookmark(Map map);

    public void delete_bookmark();

    public void delete_bibtex();

    public List<Pair> select_bibtex(Map map);

    public Map get_first_bibtex(Map map);

    public void insert_bibtex(Map map);

    public void delete_tags();

    public List<Pair> select_tags(Map map);

    public void insert_tags(Map map);
}
