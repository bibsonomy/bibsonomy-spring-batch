package de.unihannover.weblab.tasks.tagtagmasterslave;

import de.unihannover.weblab.service.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents the batch job "batch_popular_masterslave.pl" from Bibsonomy.
 *
 * Documentation from perl script:
 * This script generates the counters for the "tagtag" table of BibSonomy.
 * This special version reads from the SLAVE and writes to the MASTER.
 *
 * Environment variables:
 *   SLAVE and MASTER config variables set @see Common.pm
 *
 * Changes:
 *   2011-06-28 (rja)
 *   - using Common.pm now
 *   2011-06-27 (rja)
 *   - all database configuration variables are now read via
 *     environment variables (e.g., MASTER_HOST, MASTER_USER, ...)
 *   2008-01-23: (rja)
 *   - initial version
 * Created by sinan on 06.01.16.
 */
public class TagTagMasterslaveTasklet implements Tasklet, JobParametersIncrementer {

    private long current_content_id = 0;
    private List<String> tags_of_a_post = new ArrayList<>();
    private Map<String, Long> tagtag_ctr_hash = new HashMap<>();

    /**
     * The accompanying job will run this method.
     * In the ChunkContext are the job parameters. There are only the id and the cron expression.
     * This method initialize the connection to the database and runs all methods for the batch.
     *
     * @param stepContribution
     * @param chunkContext
     * @return
     * @throws Exception
     */
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        Map<String, Object> jobParameters = chunkContext.getStepContext().getJobParameters();
        addJobParameter(jobParameters); // do nothing, only routine

        SqlSession slave = MyBatisUtil.getSlaveSqlSessionFactory().openSession();
        SqlSession master = MyBatisUtil.getMasterSqlSessionFactory().openSession();
        try{
            executeTagNames(slave, master);
        }finally{
            slave.close();
            master.close();
        }
        return RepeatStatus.FINISHED;
    }

    /**
     * execute statements (Tag Names)
     *
     * @param slave
     * @param master
     * @throws SQLException
     */
    private void executeTagNames(SqlSession slave, SqlSession master) throws SQLException {
        //# get post to compute tag coocurence
        slave.select("select_tag_names", new TagTagResultHandler() {
            @Override
            public void execute(Map tt) {
                String tag_name = (String) tt.get("tag_name"); // tas[0]
                long content_id = (long) tt.get("content_id"); // tas[1]
                int group = (int) tt.get("group"); // tas[2]

                // next unless ($tas[2]==0);
                if (group == 0) {
                    if (current_content_id != content_id) {
//                      # start with a new post
//                      # change counter for the tags of this post

                        update_hash();
                        current_content_id = content_id;
                        tags_of_a_post.clear();
                        tags_of_a_post.add(tag_name);
                    }
                    else
                        tags_of_a_post.add(tag_name);
                }
            }
        }); // runs for every obj execute

        //#update the tagtag hash counters for the last post
        update_hash();
        slave.commit();

        slave.select("select_tagtag", new TagTagResultHandler() {
            @Override
            public void execute(Map tt) {
                String tt0 = (String) tt.get("t1"); // tas[0]
                String tt1 = (String) tt.get("t2"); // tas[1]
                Long ctr = (Long) tt.get("ctr_public"); // tas[2]

                // # check for a new tagtag counter value in the tas
                if (tagtag_ctr_hash.containsKey(tt0 + "|#|" + tt1)){
                    if (tagtag_ctr_hash.get(tt0 + "|#|" + tt1) == ctr) {
                        // # no change of the value us needed
                        // # remove it from the hash
                        tagtag_ctr_hash.remove(tt0 + "|#|" + tt1);
                    }
                } else {
                    // # all remaining hashes in $tagtag_ctr_hash are new or have new value
                    // # What happens with values which are not in $tagtag_ctr_hash but have currently a tagtag value >0
                    if (ctr > 0) {
                        tagtag_ctr_hash.put(tt0 + "|#|" + tt1, 0L);
                    }
                }

            }
        }); // runs for every obj execute
        slave.commit();

        // # MASTER
        // # set character set to utf8
        master.getConnection().prepareStatement("SET character_set_connection='utf8'").execute();

        for (String key : this.tagtag_ctr_hash.keySet()){
            String[] line = key.split("\\|#\\|");
            Map<String, String> parameters = new HashMap<>();
            parameters.put("ctr_public", ""+ this.tagtag_ctr_hash.get(key));
            parameters.put("t1", "\"" + line[0] + "\"");
            parameters.put("t2", "\"" + line[1] + "\"");
            int number = master.update("update_tag", parameters);

            if (number == 0){ // number == 0E0
                parameters = new HashMap<>();
                parameters.put("ctr_public", ""+ this.tagtag_ctr_hash.get(key));
                parameters.put("t1", "\"" + line[0] + "\"");
                parameters.put("t2", "\"" + line[1] + "\"");
                master.update("insert_tag_tag", parameters);
            }

        }

        master.commit();
    }

    /**
     * subroutines
     */
    private void update_hash() {
        for (String tag1 : this.tags_of_a_post){
            for (String tag2 : this.tags_of_a_post) {
                if (!tag1.equals(tag2)) {
                    if (tagtag_ctr_hash.containsKey(tag1 + "|#|" + tag2)) {
                        Long l = tagtag_ctr_hash.get(tag1 + "|#|" + tag2);
                        l++;
                        tagtag_ctr_hash.put(tag1 + "|#|" + tag2, l);
                    } else
                        tagtag_ctr_hash.put(tag1 + "|#|" + tag2, 1L);
                }
            }
        }
    }

    /**
     * This method is only routine implemented. It does nothing.
     * If this tasklet needs parameter, then this method must be filled.
     *
     * @param jobParameters
     */
    private void addJobParameter(Map<String, Object> jobParameters) {
        // nothing to add
    }


    /**
     * This method is the override of the JobParametersIncrementer interface.
     * It sets a id which is the time, because the job parameter have to be unique.
     *
     * @param jobParameters
     * @return JobParameters
     */
    @Override
    public JobParameters getNext(JobParameters jobParameters) {
        long id = System.currentTimeMillis();
        JobParameters parameters = new JobParametersBuilder()
                .addLong("run.time", id)
                .addString("cron_expression", jobParameters.getString("cron_expression", ""))
                .toJobParameters();
        return parameters;
    }

}
