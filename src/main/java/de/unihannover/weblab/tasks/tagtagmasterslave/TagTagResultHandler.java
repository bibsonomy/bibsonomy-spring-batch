package de.unihannover.weblab.tasks.tagtagmasterslave;

import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;

import java.util.Map;

/**
 * This class handles the returning from a SQL query.
 * It runs for every row handleResult. The object is given in a map.
 *
 * It works like a cursor.
 * The based idea of this class is that no object have to be saved in a list.
 *
 * Created by sinan on 06.01.16.
 */
public abstract class TagTagResultHandler implements ResultHandler {

    /**
     * For every object / row this method runs execute.
     * @param resultContext
     */
    @Override
    public void handleResult(ResultContext resultContext) {
        Map obj = (Map) resultContext.getResultObject();
        execute(obj);
    }

    /**
     * This method process a job.
     * @param tt
     */
    public abstract void execute(Map tt);
}
