package de.unihannover.weblab.tasks.tagtagmasterslave.mappers;

import org.apache.ibatis.session.ResultHandler;

import java.util.List;
import java.util.Map;

/**
 * Mapper for the PopularMasterslaveJob.
 * It is important that this class is included in the mybatis config
 * and in the mapper xml file.
 *
 * Created by sinan on 06.01.16.
 */
public interface TagTagMasterslaveMapper {

    public List<Map> select_tag_name(ResultHandler resultHandler);
}
